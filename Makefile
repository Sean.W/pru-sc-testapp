# $Id: Makefile 4192 2009-05-10 15:48:14Z rousseau $

# PC/SC Lite libraries and headers.
# by default install in /usr/local
DESTDIR ?= /usr/local

BIN = pcsc_test

all: $(BIN)

pcsc_test: pcsc_test.o

install: all
	install -d $(DESTDIR)
	install $(BIN) $(DESTDIR)

clean:
	rm -f pcsc_test.o $(BIN)

.PHONY: clean all install

