/*
 * MUSCLE SmartCard Development ( http://www.linuxnet.com )
 *
 * Copyright (C) 1999
 *  David Corcoran <corcoran@linuxnet.com>
 * Copyright (C) 2004-2010
 *  Ludovic Rousseau <ludovic.rousseau@free.fr>
 *
 * $Id: testpcsc.c 5096 2010-08-02 14:33:35Z rousseau $
 */

/**
 * @file
 * @brief This is a test program for pcsc-lite.
 */

//#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcsclite.h"
#include "winscard.h"
#include "reader.h"

#define PANIC 0
#define DONT_PANIC 1

#define FILETYPE_BINARY  		0x00
#define FILETYPE_LINEARFIXED 		0x01
#define FILETYPE_LINEARVARIABLE 	0x02
#define FILETYPE_CYCLIC 		0x03
#define FILETYPE_ATR 			0x04
#define FILETYPE_KEY 			0x05
#define FILETYPE_PURSE 			0x06
#define FILETYPE_DEPOSIT 		0x07
#define FILETYPE_SPECIALBINARY 		0x08
#define FILETYPE_PUBLICKEY 		0x0A
#define FILETYPE_PRIVATEKEY 		0x0B
#define FILETYPE_SIMPLEPURSE 		0x0C
#define FILETYPE_PLAINTEXT 		0x00
#define FILETYPE_PLAINTTEXTMAC 		0x40
#define FILETYPE_CIPHERTEXT 		0x80
#define FILETYPE_CIPHERTEXTMAC 		0xC0

#define USE_AUTOALLOCATE

#define BLUE "\33[34m"
#define RED "\33[31m"
#define BRIGHT_RED "\33[01;31m"
#define GREEN "\33[32m"
#define NORMAL "\33[0m"
#define MAGENTA "\33[35m"


unsigned char bSendBuffer[MAX_BUFFER_SIZE];
unsigned char bRecvBuffer[MAX_BUFFER_SIZE];
unsigned char bInputBuffer[MAX_BUFFER_SIZE];

void CleanGlobalBuffers();
void SendAPDU(int send_length,SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void SendEraseMF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void CreateMasterFile(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void CreateEFFile(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext,unsigned char* filelen,unsigned char* fileid,unsigned char * accesscondition,unsigned char filetype);
void InstallExternalAuth(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void InstallInternalAuth(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void CreateDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);	
void SelectDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void InstallPin(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);			
void CreateEndDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void CreateEndMF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void CleanupandExit(SCARDCONTEXT hContext,char *pcReaders,unsigned char *pbAtr,SCARDHANDLE hCard);
void ExternalAuthenticate(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext, char cardType);
void GetChalenge(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void GetInternalAuthenticanResponse(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
void RunInternalAuthenticationTest(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext);
#ifdef MULTIPLE_BAUD
void ChangeBaudRate(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext, unsigned int newBaud);
#endif /* MULTIPLE_BAUD */

static void test_rv(LONG rv, SCARDCONTEXT hContext, int dont_panic)
{
	if (rv != SCARD_S_SUCCESS)
	{
		if (dont_panic)
			printf(BLUE "%s (don't panic)\n" NORMAL, pcsc_stringify_error(rv));
		else
		{
			printf(RED "%s\n" NORMAL, pcsc_stringify_error(rv));
			(void)SCardReleaseContext(hContext);
			exit(-1);
		}
	}
	else
	{
		printf(" ");
		return;
		//(void)puts(pcsc_stringify_error(rv));
	}
}

int main(/*@unused@*/ int argc, /*@unused@*/ char **argv)
{
	unsigned char filelen[2];
	unsigned char fileid[2];
	unsigned char accesscondition[2];
#ifdef MULTIPLE_BAUD
	unsigned int  newBaud = 0;
#endif /* MULTIPLE_BAUD */

	char c = 'a';
	char cardType = 'a';
	SCARDHANDLE hCard;
	SCARDCONTEXT hContext;
	SCARD_READERSTATE rgReaderStates[1];
	DWORD dwReaderLen, dwState, dwProt, dwAtrLen;
	DWORD dwPref, dwReaders = 0;
	char *pcReaders = NULL, *mszReaders;
#ifdef USE_AUTOALLOCATE
	unsigned char *pbAtr = NULL;
#else
	unsigned char pbAtr[MAX_ATR_SIZE];
#endif
	/*union {
		unsigned char as_char[100];
		DWORD as_DWORD;
		uint32_t as_uint32_t;
	} buf;*/
	//DWORD dwBufLen;
	unsigned char *pbAttr = NULL;
	DWORD pcbAttrLen;
	char *mszGroups;
	DWORD dwGroups = 0;
	
	DWORD i;
	int p, iReader;
	int iList[16];
	SCARD_IO_REQUEST pioRecvPci;
	SCARD_IO_REQUEST pioSendPci;
	long rv;

    int offset;
    unsigned int tempbuff[8];
	
	//DWORD send_length =0;//, length;

	(void)argc;
	(void)argv;

	printf(MAGENTA "TI Smartcard reader test application...\n\n" NORMAL);
	
	//printf("Testing SCardEstablishContext\t: ");
	rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext);
	test_rv(rv, hContext, PANIC);

	//printf("Testing SCardIsValidContext\t: ");
	rv = SCardIsValidContext(hContext);
	test_rv(rv, hContext, PANIC);

/*	//printf("Testing SCardIsValidContext\t: ");
	rv = SCardIsValidContext(hContext+1);
	test_rv(rv, hContext, DONT_PANIC);
*/
	//printf("Testing SCardListReaderGroups\t: ");
#ifdef USE_AUTOALLOCATE
	dwGroups = SCARD_AUTOALLOCATE;
	rv = SCardListReaderGroups(hContext, (LPSTR)&mszGroups, &dwGroups);
#else
	rv = SCardListReaderGroups(hContext, NULL, &dwGroups);
	test_rv(rv, hContext, PANIC);

	//printf("Testing SCardListReaderGroups\t: ");
	mszGroups = calloc(dwGroups, sizeof(char));
	rv = SCardListReaderGroups(hContext, mszGroups, &dwGroups);
#endif
	test_rv(rv, hContext, PANIC);

	/*
	 * Have to understand the multi-string here
	 */
	p = 0;
	for (i = 0; i+1 < dwGroups; i++)
	{
		++p;
		//printf(GREEN "Group %02d: %s\n" NORMAL, p, &mszGroups[i]);
		while (mszGroups[++i] != 0) ;
	}

#ifdef USE_AUTOALLOCATE
	//printf("Testing SCardFreeMemory\t\t: ");
	rv = SCardFreeMemory(hContext, mszGroups);
	test_rv(rv, hContext, PANIC);
#else
	free(mszGroups);
#endif

wait_for_card_again:
	mszGroups = NULL;
	//printf("Testing SCardListReaders\t: ");
	rv = SCardListReaders(hContext, mszGroups, NULL, &dwReaders);
	test_rv(rv, hContext, DONT_PANIC);
	if (SCARD_E_NO_READERS_AVAILABLE == rv)
	{
		printf("Testing SCardGetStatusChange \n");
		printf("Please insert a working reader\t: ");
		(void)fflush(stdout);
		rgReaderStates[0].szReader = "\\\\?PnP?\\Notification";
		rgReaderStates[0].dwCurrentState = SCARD_STATE_EMPTY;

		rv = SCardGetStatusChange(hContext, INFINITE, rgReaderStates, 1);
		test_rv(rv, hContext, PANIC);
	}

	//printf("Testing SCardListReaders\t: ");
#ifdef USE_AUTOALLOCATE
	dwReaders = SCARD_AUTOALLOCATE;
	rv = SCardListReaders(hContext, mszGroups, (LPSTR)&mszReaders, &dwReaders);
#else
	rv = SCardListReaders(hContext, mszGroups, NULL, &dwReaders);
	test_rv(rv, hContext, PANIC);

	printf("Testing SCardListReaders\t: ");
	mszReaders = calloc(dwReaders, sizeof(char));
	rv = SCardListReaders(hContext, mszGroups, mszReaders, &dwReaders);
#endif
	test_rv(rv, hContext, DONT_PANIC);

	/*
	 * Have to understand the multi-string here
	 */
	p = 0;
	for (i = 0; i+1 < dwReaders; i++)
	{
		++p;
		printf(GREEN "Reader %02d: %s\n" NORMAL, p, &mszReaders[i]);
		iList[p] = i;
		while (mszReaders[++i] != 0) ;
	}

	if (p > 1)
		do
		{
			char input[80];

			printf("Enter the reader number\t\t: ");
			(void)fgets(input, sizeof(input), stdin);
			(void)sscanf(input, "%d", &iReader);

			if (iReader > p || iReader <= 0)
				printf("Invalid Value - try again\n");
		}
		while (iReader > p || iReader <= 0);
	else
		iReader = 1;

	rgReaderStates[0].szReader = &mszReaders[iList[iReader]];
	rgReaderStates[0].dwCurrentState = SCARD_STATE_EMPTY;

	printf("Waiting for card insertion\t: ");
	(void)fflush(stdout);
	rv = SCardGetStatusChange(hContext, INFINITE, rgReaderStates, 1);
	test_rv(rv, hContext, PANIC);
	if (rgReaderStates[0].dwEventState & SCARD_STATE_UNKNOWN)
	{
		printf("\nA reader has been connected/disconnected\n");
		goto wait_for_card_again;
	}

	//printf("Testing SCardConnect\t\t: ");
	rv = SCardConnect(hContext, &mszReaders[iList[iReader]],
		SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,
		&hCard, &dwPref);
	test_rv(rv, hContext, PANIC);

	switch(dwPref)
	{
		case SCARD_PROTOCOL_T0:
			pioSendPci = *SCARD_PCI_T0;
			break;
		case SCARD_PROTOCOL_T1:
			pioSendPci = *SCARD_PCI_T1;
			break;
		case SCARD_PROTOCOL_RAW:
			pioSendPci = *SCARD_PCI_RAW;
			break;
		default:
			printf("Unknown protocol\n");
			return -1;
	}
/*
	//printf("Testing SCardControl\t\t: ");
#ifdef PCSC_PRE_120
	{
		char buffer[1024] = "Foobar";
		DWORD cbRecvLength = sizeof(buffer);

		rv = SCardControl(hCard, buffer, 7, buffer, &cbRecvLength);
	}
#else
	{
		char buffer[1024] = { 0x02 };
		DWORD cbRecvLength = sizeof(buffer);

		rv = SCardControl(hCard, SCARD_CTL_CODE(1), buffer, 1, buffer,
			sizeof(buffer), &cbRecvLength);
		if (cbRecvLength && (SCARD_S_SUCCESS == rv))
		{
			for (i=0; i<cbRecvLength; i++)
				printf("%c", buffer[i]);
			printf(" ");
		}
	}
#endif
	test_rv(rv, hContext, DONT_PANIC);
*/
	//printf("Testing SCardGetAttrib\t\t: ");
#ifdef USE_AUTOALLOCATE
	pcbAttrLen = SCARD_AUTOALLOCATE;
	rv = SCardGetAttrib(hCard, SCARD_ATTR_DEVICE_FRIENDLY_NAME, (unsigned char *)&pbAttr,
		&pcbAttrLen);
#else
	rv = SCardGetAttrib(hCard, SCARD_ATTR_DEVICE_FRIENDLY_NAME, NULL, &pcbAttrLen);
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
	{
		printf("SCARD_ATTR_DEVICE_FRIENDLY_NAME length: " GREEN "%ld\n" NORMAL, pcbAttrLen);
		pbAttr = malloc(pcbAttrLen);
	}

	printf("Testing SCardGetAttrib\t\t: ");
	rv = SCardGetAttrib(hCard, SCARD_ATTR_DEVICE_FRIENDLY_NAME, pbAttr, &pcbAttrLen);
#endif
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
		printf("SCARD_ATTR_DEVICE_FRIENDLY_NAME: " GREEN "%s\n" NORMAL, pbAttr);

#ifdef USE_AUTOALLOCATE
	//printf("Testing SCardFreeMemory\t\t: ");
	rv = SCardFreeMemory(hContext, pbAttr);
	test_rv(rv, hContext, PANIC);
#else
	if (pbAttr)
		free(pbAttr);
#endif

	//printf("Testing SCardGetAttrib\t\t: ");
#ifdef USE_AUTOALLOCATE
	pcbAttrLen = SCARD_AUTOALLOCATE;
	rv = SCardGetAttrib(hCard, SCARD_ATTR_ATR_STRING, (unsigned char *)&pbAttr,
		&pcbAttrLen);
#else
	rv = SCardGetAttrib(hCard, SCARD_ATTR_ATR_STRING, NULL, &pcbAttrLen);
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
	{
		printf("SCARD_ATTR_ATR_STRING length: " GREEN "%ld\n" NORMAL, pcbAttrLen);
		pbAttr = malloc(pcbAttrLen);
	}

	//printf("Testing SCardGetAttrib\t\t: ");
	rv = SCardGetAttrib(hCard, SCARD_ATTR_ATR_STRING, pbAttr, &pcbAttrLen);
#endif
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
	{
		printf("SCARD_ATTR_ATR_STRING length: " GREEN "%ld\n" NORMAL, pcbAttrLen);
		printf("SCARD_ATTR_ATR_STRING: " GREEN);
		for (i = 0; i < pcbAttrLen; i++)
			printf("%02X ", pbAttr[i]);
		printf("\n" NORMAL);
	}

#ifdef USE_AUTOALLOCATE
	//printf("Testing SCardFreeMemory\t\t: ");
	rv = SCardFreeMemory(hContext, pbAttr);
	test_rv(rv, hContext, PANIC);
#else
	if (pbAttr)
		free(pbAttr);
#endif
/*
	printf("Testing SCardGetAttrib\t\t: ");
	dwBufLen = sizeof(buf);
	rv = SCardGetAttrib(hCard, SCARD_ATTR_VENDOR_IFD_VERSION, buf.as_char, &dwBufLen);
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
		printf("Vendor IFD version\t\t: " GREEN "0x%08lX\n" NORMAL,
			buf.as_DWORD);

	printf("Testing SCardGetAttrib\t\t: ");
	dwBufLen = sizeof(buf);
	rv = SCardGetAttrib(hCard, SCARD_ATTR_MAXINPUT, buf.as_char, &dwBufLen);
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
	{
		if (dwBufLen == sizeof(uint32_t))
			printf("Max message length\t\t: " GREEN "%d\n" NORMAL,
				buf.as_uint32_t);
		else
			printf(RED "Wrong size" NORMAL);
	}

	printf("Testing SCardGetAttrib\t\t: ");
	dwBufLen = sizeof(buf);
	rv = SCardGetAttrib(hCard, SCARD_ATTR_VENDOR_NAME, buf.as_char, &dwBufLen);
	test_rv(rv, hContext, DONT_PANIC);
	if (rv == SCARD_S_SUCCESS)
		printf("Vendor name\t\t\t: " GREEN "%s\n" NORMAL, buf.as_char);

	printf("Testing SCardSetAttrib\t\t: ");
	rv = SCardSetAttrib(hCard, SCARD_ATTR_ATR_STRING, (LPCBYTE)"", 1);
	test_rv(rv, hContext, DONT_PANIC);
*/
	//printf("Testing SCardStatus\t\t: ");

#ifdef USE_AUTOALLOCATE
	dwReaderLen = SCARD_AUTOALLOCATE;
	dwAtrLen = SCARD_AUTOALLOCATE;
	rv = SCardStatus(hCard, (LPSTR)&pcReaders, &dwReaderLen, &dwState, &dwProt,
		(LPBYTE)&pbAtr, &dwAtrLen);
#else
	dwReaderLen = 100;
	pcReaders   = malloc(sizeof(char) * 100);
	dwAtrLen    = MAX_ATR_SIZE;

	rv = SCardStatus(hCard, pcReaders, &dwReaderLen, &dwState, &dwProt,
		pbAtr, &dwAtrLen);
#endif
	test_rv(rv, hContext, PANIC);

	printf("Current Reader Name\t\t: " GREEN "%s\n" NORMAL, pcReaders);
	printf("Current Reader State\t\t: " GREEN "0x%.4lx\n" NORMAL, dwState);
	printf("Current Reader Protocol\t\t: T=" GREEN "%ld\n" NORMAL, dwProt - 1);
	printf("Current Reader ATR Size\t\t: " GREEN "%ld" NORMAL " bytes\n",
		dwAtrLen);
	printf("Current Reader ATR Value\t: " GREEN);

	for (i = 0; i < dwAtrLen; i++)
	{
		printf("%02X ", pbAtr[i]);
	}
	printf(NORMAL "\n");

//////////Card Read/write test - Rishi///////////////////////////////////////////////////////////////////////////
	/* APDU select file */
    printf("\nSend Erase MF command? [y/n] ");

    c = getchar();

    if (c == 'Y' || c == 'y' )
    {
        SendEraseMF(pioRecvPci,pioSendPci,rv,hCard,hContext);
    }

    // Clean out the newline in STDIN
    c = getchar();

    CreateMasterFile(pioRecvPci,pioSendPci,rv,hCard,hContext);
    if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
        printf(GREEN "Successfully created MF File\n");
    else
    {
        printf(RED "Failed to create MF\n"); 
        CleanupandExit(hContext,pcReaders,pbAtr,hCard);
        return 0;
    }

	filelen[0] = 0x05;filelen[1] = 0x00;
	fileid[0] = 00;fileid[1] = 01;	
	accesscondition[0] = 0x0F;accesscondition[1] = 0x00;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_KEY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF File\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	InstallExternalAuth(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully installed external authentication\n");
	else
	{
		printf(RED "Failed to install external authentication\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}
	
	InstallInternalAuth(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully installed internal authentication\n");
	else
	{
		printf(RED "Failed to install internal authentication\n"); 
  		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	CreateDF(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created DF\n");
	else
	{
		printf(RED "Failed to create DF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	SelectDF(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x61)
		printf(GREEN "Successfully selected DF\n");
	else
	{
		printf(RED "Failed to select DF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	filelen[0] = 0x04;filelen[1] = 0x00;
	fileid[0] = 0x00;fileid[1] = 0x01;	
	accesscondition[0] = 0x5F;accesscondition[1] = 0x00;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_KEY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	InstallExternalAuth(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully installed external authentication\n");
	else
	{
		printf(RED "Failed to install external authentication\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	InstallPin(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully installed PIN\n");
	else
	{
		printf(RED "Failed to install PIN\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	filelen[0] = 0x00;filelen[1] = 0x28;
	fileid[0] = 0x00;fileid[1] = 0x15;	
	accesscondition[0] = 0x0F;accesscondition[1] = 0x22;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_BINARY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	filelen[0] = 0x00;filelen[1] = 0x64;
	fileid[0] = 0x00;fileid[1] = 0x16;	
	accesscondition[0] = 0x0F;accesscondition[1] = 0x22;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_BINARY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	filelen[0] = 0x00;filelen[1] = 0x77;
	fileid[0] = 0x00;fileid[1] = 0x18;	
	accesscondition[0] = 0x22;accesscondition[1] = 0xF0;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_BINARY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	filelen[0] = 0x00;filelen[1] = 0x64;
	fileid[0] = 0x00;fileid[1] = 0x19;	
	accesscondition[0] = 0x33;accesscondition[1] = 0xF0;
	CreateEFFile(pioRecvPci,pioSendPci,rv,hCard,hContext,filelen,fileid,accesscondition,FILETYPE_BINARY);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created EF\n");
	else
	{
		printf(RED "Failed to create EF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	CreateEndDF(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created end of DF\n");
	else
	{
		printf(RED "Failed to create end DF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	CreateEndMF(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
		printf(GREEN "Successfully created end of MF\n");
	else
	{
		printf(RED "Failed to create end MF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}

	RunInternalAuthenticationTest(pioRecvPci,pioSendPci,rv,hCard,hContext);
	if (bRecvBuffer[0] == 0x61)
	{
        offset = bRecvBuffer[1];
		GetInternalAuthenticanResponse(pioRecvPci,pioSendPci,rv,hCard,hContext);
		{
			if (bRecvBuffer[offset] == 0x90 && bRecvBuffer[offset+1] == 0x00)
			{
				printf(GREEN "Successfully completed internal authentication test...\n");
			}
			else
			{
				printf(RED "Failed internal authentication test...\n"); 
				CleanupandExit(hContext,pcReaders,pbAtr,hCard);
				return 0;
			}
		}	
	}
	else
	{
		printf(RED "Failed to send internal authentication APDU MF\n"); 
		CleanupandExit(hContext,pcReaders,pbAtr,hCard);
		return 0;
	}
#ifdef MULTIPLE_BAUD
    printf("Please enter the baudrate :");
    scanf("%d", &newBaud);
    
    // Tell the SmartCard to Change the BaudRate
    ChangeBaudRate(pioRecvPci,pioSendPci,rv,hCard,hContext, newBaud);
    if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00) {
        printf(GREEN "Successfully Changed SmartCard BaudRate...\n");

	//Tell the Reader to change the BaudRate
	rv = SCardControl(hCard, SCARD_CTL_CODE(2052), (char *)&newBaud, sizeof(unsigned int), 
			(char *)&newBaud, sizeof(unsigned int),&cbRecvLength);
	if (SCARD_S_SUCCESS == rv)
           printf(GREEN "Successfully Changed Reader BaudRate...\n");
	else
           printf(RED "Failed Changing Reader BaudRate ...\n"); 
    } else {
            printf(RED "Failed Changing SmartCard BaudRate ...\n"); 
	    CleanupandExit(hContext,pcReaders,pbAtr,hCard);
	    return 0;
    }
#endif /* MULTIPLE_BAUD */

	//External Authentication test
    printf("\nExternal Authentication test\n");
    printf("\tNOTE: This will require the user to decrypt the challenge\n");
    printf("\t      bytes themselves with 3EDS and provide the value to\n");
    printf("\t      the application\n");
    printf("\nPerform External Authentication test? [y/n] ");

    c = getchar();

    if (c == 'Y' || c == 'y' )
    {
        c = getchar();

        printf("\nIs this test running on DIP package or CARD? [d/c] ");
        cardType = getchar();
        if((cardType == 'd') || (cardType == 'D') || (cardType == 'c') || (cardType == 'C')) 
        {
            GetChalenge(pioRecvPci,pioSendPci,rv,hCard,hContext);
            if (bRecvBuffer[8] == 0x90 && bRecvBuffer[9] == 0x00)
            {
                printf("\nChallenge from Card:" GREEN);
                for (i=0; i < 8; i++) {
                    printf(" %02X", bRecvBuffer[i]);
                }

                // Clean out the newline in STDIN
                c = getchar();

                printf("\nPlease enter the decrypted value of the challenge: ");
                scanf("%02x%02x%02x%02x%02x%02x%02x%02x", &tempbuff[0],
                        &tempbuff[1], &tempbuff[2], &tempbuff[3],
                        &tempbuff[4], &tempbuff[5], &tempbuff[6],
                        &tempbuff[7]);

                //Convert to bytes
                for (i=0; i < 8; i++) {
                    bInputBuffer[i] = (unsigned char)tempbuff[i];
                }

                printf("\nInput from user:" GREEN);
                for (i=0; i < 8; i++) {
                    printf(" %02X", bInputBuffer[i]);
                }

                ExternalAuthenticate(pioRecvPci,pioSendPci,rv,hCard,hContext,cardType);
                if (bRecvBuffer[0] == 0x90 && bRecvBuffer[1] == 0x00)
                {
                    printf(GREEN "Successfully completed external authentication test...\n");
                }
                else
                {
                    printf(RED "Failed external authentication test...\n"); 
                    CleanupandExit(hContext,pcReaders,pbAtr,hCard);
                    return 0;
                }
            }
            else {
                printf(RED "Failed to send getchallenge apdu...\n"); 
                CleanupandExit(hContext,pcReaders,pbAtr,hCard);
                return 0;
            }
        }
        else
        {
            printf(RED "Invalid HArdware Type...\n"); 
            CleanupandExit(hContext,pcReaders,pbAtr,hCard);
            return 0;
        }
    }

    // Clean out the newline in STDIN
    c = getchar();

//////////Card Read/write test - Rishi///////////////////////////////////////////////////////////////////////////
	CleanupandExit(hContext,pcReaders,pbAtr,hCard);
	return 0;
}

	void CleanupandExit(SCARDCONTEXT hContext,char *pcReaders,unsigned char *pbAtr,SCARDHANDLE hCard)
	{
		long rv;
		#ifdef USE_AUTOALLOCATE
		//printf("Testing SCardFreeMemory\t\t: ");
		rv = SCardFreeMemory(hContext, pcReaders);
		test_rv(rv, hContext, PANIC);
		//printf("Testing SCardFreeMemory\t\t: ");
		rv = SCardFreeMemory(hContext, pbAtr);
		test_rv(rv, hContext, PANIC);
	#else
		if (pcReaders)
			free(pcReaders);
	#endif

		if (rv != SCARD_S_SUCCESS)
		{
			(void)SCardDisconnect(hCard, SCARD_RESET_CARD);
			(void)SCardReleaseContext(hContext);
		}
/*
		//printf("Press enter: ");
		//(void)getchar();
		//printf("Testing SCardReconnect\t\t: ");
		rv = SCardReconnect(hCard, SCARD_SHARE_SHARED,
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, SCARD_UNPOWER_CARD, &dwPref);
		test_rv(rv, hContext, PANIC);

		//printf("Testing SCardDisconnect\t\t: ");
		rv = SCardDisconnect(hCard, SCARD_UNPOWER_CARD);
		test_rv(rv, hContext, PANIC);

	#ifdef USE_AUTOALLOCATE
		//printf("Testing SCardFreeMemory\t\t: ");
		rv = SCardFreeMemory(hContext, mszReaders);
		test_rv(rv, hContext, PANIC);
	#else
		free(mszReaders);
	#endif
*/
		//printf("Testing SCardReleaseContext\t: ");
		rv = SCardReleaseContext(hContext);
		test_rv(rv, hContext, PANIC);

		printf("\n");
		printf(NORMAL "Test Completed ... !\n");
	}

	void SendAPDU(int send_length,SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int i=0;
		DWORD length =0;
		//uses globals sendbuffer and receive buffer ...
		for (i=0; i<send_length; i++)
			printf(" %02X", bSendBuffer[i]);
		printf("\n");
		length = sizeof(bRecvBuffer);

		rv = SCardTransmit(hCard, &pioSendPci, bSendBuffer, send_length,
			&pioRecvPci, bRecvBuffer, &length);
		test_rv(rv, hContext, PANIC);
		printf(" card response:" GREEN);
		for (i=0; i<length; i++)
			printf(" %02X", bRecvBuffer[i]);
		printf("\n" NORMAL);
		return ; 
	}

	void CleanGlobalBuffers()
	{
		int i=0;
		for(i=0;i<MAX_BUFFER_SIZE;i++)
			bSendBuffer[i] =0;

		for(i=0;i<MAX_BUFFER_SIZE;i++)
			bRecvBuffer[i] =0;
	}

	void SendEraseMF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Erase MF APDU");
		send_length = 13;
		memcpy(bSendBuffer, "\x80\x0E\x00\x00\x08\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}
	
	void CreateMasterFile(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Create MF APDU");
		send_length = 29;
		memcpy(bSendBuffer, "\x80\xE0\x00\x00\x18\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF\x0F\x00\x31\x50\x41\x59\x2E\x53\x59\x53\x2E\x44\x44\x46\x30\x31", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void CreateEFFile(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext,unsigned char* filelen,unsigned char* fileid,unsigned char * accesscondition,unsigned char filetype)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Create EF file APDU, FileLen = %2x%2x, FileID= %2x%2x, accesscondition= %2x%2x, filetype = %2x",filelen[0],filelen[1],fileid[0],fileid[1],accesscondition[0],accesscondition[1],filetype);
		
		bSendBuffer[0] = 0x80;
		bSendBuffer[1] = 0xE0;
		bSendBuffer[2] = 0x02;
		bSendBuffer[3] = 0x00;
		bSendBuffer[4] = 0x07;
		bSendBuffer[5] = fileid[0]; 
		bSendBuffer[6] = fileid[1]; 
		bSendBuffer[7] = filetype;
		bSendBuffer[8] = accesscondition[0]; 
		bSendBuffer[9] = accesscondition[1]; 
		bSendBuffer[10] = filelen[0]; 
		bSendBuffer[11] = filelen[1]; 

		send_length = 12;
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void InstallExternalAuth(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending InstallExternalAuth APDU");
		send_length = 29;
		memcpy(bSendBuffer, "\x80\xD4\x00\x00\x18\x01\x01\x00\x08\x0F\x05\x5F\xFF\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void InstallInternalAuth(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending InstallInternalAuth APDU");
		send_length = 29;
		memcpy(bSendBuffer, "\x80\xD4\x00\x00\x18\x01\x01\x00\x09\x0F\x05\x5F\xFF\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void CreateDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Create DF APDU");
		send_length = 20;
		memcpy(bSendBuffer, "\x80\xE0\x01\x00\x0F\xDF\x01\x0F\x00\xBC\xDC\xBC\xD3\x45\x2E\x44\x44\x46\x30\x31", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void SelectDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Select DF APDU");
		send_length = 7;
		memcpy(bSendBuffer, "\x00\xA4\x00\x00\x02\xDF\x01", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void InstallPin(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending Install PIN APDU");
		send_length = 17;
		memcpy(bSendBuffer, "\x80\xD4\x00\x00\x0C\x01\x01\x00\x0B\x0F\x03\x33\x66\x00\x00\x00\x00", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void CreateEndDF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending end DF APDU");
		send_length = 7;
		memcpy(bSendBuffer, "\x80\xE0\x01\x01\x02\xDF\x01", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void CreateEndMF(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending end MF APDU");
		send_length = 7;
		memcpy(bSendBuffer, "\x80\xE0\x00\x01\x02\x3F\x00", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}
	
	void RunInternalAuthenticationTest(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending internal authentication APDU");
		send_length = 13;
		memcpy(bSendBuffer, "\x00\x88\x00\x01\x08\x11\x22\x33\x44\x55\x66\x77\x88", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void GetInternalAuthenticanResponse(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Geting internal authentication response APDU");
		send_length = 5;
		memcpy(bSendBuffer, "\x00\xc0\x00\x00\x08", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void GetChalenge(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext)
	{
		int send_length =0;
		CleanGlobalBuffers();
		printf(NORMAL "\n Sending GetChallenge APDU");
		send_length = 5;
		memcpy(bSendBuffer, "\x00\x84\x00\x00\x08", send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

	void ExternalAuthenticate(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext, char cardType)
	{
		int send_length = 13;
        unsigned char temp[send_length];
		CleanGlobalBuffers();

        //Fill in the temp buffer with APDU command
        temp[0] = 0x00;
        temp[1] = 0x82;
        temp[2] = 0x00;

        if((cardType == 'd') || (cardType == 'D')) 
            temp[3] = 0x01;  // For DIP
        else
            temp[3] = 0x00;  // For Card
    
        temp[4] = 0x08;
        temp[5] = bInputBuffer[0];
        temp[6] = bInputBuffer[1];
        temp[7] = bInputBuffer[2];
        temp[8] = bInputBuffer[3];
        temp[9] = bInputBuffer[4];
        temp[10] = bInputBuffer[5];
        temp[11] = bInputBuffer[6];
        temp[12] = bInputBuffer[7];

		printf(NORMAL "\n Sending external authenticate APDU");
//		memcpy(bSendBuffer, "\x00\x82\x00\x00\x08\x9D\xEA\x56\x54\x74\x36\x0B\x55", send_length);
		memcpy(bSendBuffer, temp, send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}

#ifdef MULTIPLE_BAUD
	void ChangeBaudRate(SCARD_IO_REQUEST pioRecvPci,SCARD_IO_REQUEST pioSendPci,long rv,SCARDHANDLE hCard,SCARDCONTEXT hContext, unsigned int newBaud)
	{
		int send_length = 10;
        unsigned char temp[send_length];
		CleanGlobalBuffers();

        //Fill in the temp buffer with APDU command
        temp[0] = 0xFF;
        temp[1] = 0x00;
        temp[2] = 0x00;
        temp[3] = 0x00;
        temp[4] = 0x05;
        temp[5] = 0xD4;
        temp[6] = 0x4E;
        temp[7] = 0x01;
        temp[8] = 0xFF; // This value will change for different baud rate, card Specific
        temp[9] = 0xFF; // This value will change for different baud rate, card Specific

		printf(NORMAL "\n Sending Change Baud APDU");
		memcpy(bSendBuffer, temp, send_length);
		SendAPDU(send_length,pioRecvPci,pioSendPci,rv,hCard,hContext);
	}
#endif
